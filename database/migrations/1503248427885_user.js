'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('username', 80).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.string('first_name', 50).notNullable()
      table.string('middle_name', 20)
      table.string('last_name', 50).notNullable()
      table.integer('age').notNullable()
      table.string('address', 254)
      table.string('password', 60).notNullable()
      table.timestamps()
      // table.timestamps('created_at').defaultTo(this.fn.now())
      // table.timestamps('updated_at').defaultTo(this.fn.now())
      // table.timestamps('deleted_at').defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
