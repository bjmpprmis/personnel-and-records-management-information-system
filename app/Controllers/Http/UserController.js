'use strict'
const User = use('App/Models/User')

class UserController {

    async index ({ request, response, view }) {
        const user = await User.all()
        
        return view.render('user', { users: user.toJSON() })
        // return await User.all()
    }
}

module.exports = UserController
